//
//  BottomViewSheet.swift
//  BottomSheetDemo
//
//  Created by Purnendu Roy on 29/03/20.
//  Copyright © 2020 Purnendu Roy. All rights reserved.
//

import UIKit

extension BottomViewSheet {
    private enum State {
        case partial
        case full
    }
    
    private enum Constant {
        static let fullViewYPosition: CGFloat = 100
        static var partialViewYPosition: CGFloat { UIScreen.main.bounds.height - 250 }
    }
}

class BottomViewSheet: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            self.tableView.backgroundColor = UIColor.white
            self.tableView.isScrollEnabled = false
        }
    }
    var scrollRecognizer = UITapGestureRecognizer()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupPanGesture()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.initialAnimation()
    }
    
    private func initialAnimation() {
        UIView.animate(withDuration: 0.6, animations: { [weak self] in
            let frame = self?.view.frame
            let yComponent = Constant.partialViewYPosition
            self?.view.frame = CGRect(x: 0, y: yComponent, width: frame!.width, height: frame!.height)
        })
    }
    
    func getPartialViewYPosition() {
        
    }
    
    func getFullViewYPostion() {
        
    }
    
    //MARK: - Gesture
    private func setupPanGesture() {
        let gesture = UIPanGestureRecognizer.init(target: self, action: #selector(panGesture))
        self.view.addGestureRecognizer(gesture)
    }
    
    @objc private func panGesture(_ recognizer: UIPanGestureRecognizer) {
        let translation = recognizer.translation(in: self.view)
        let velocity = recognizer.velocity(in: self.view)
        let minY = self.view.frame.minY
        if (minY + translation.y >= Constant.fullViewYPosition) && (minY + translation.y <= Constant.partialViewYPosition) {
            self.view.frame = CGRect(x: 0, y: minY + translation.y, width: self.view.frame.width, height: self.view.frame.height)
            recognizer.setTranslation(CGPoint.zero, in: self.view)
        }
        
        if recognizer.state == .ended {
            var duration =  velocity.y < 0 ? Double((minY - Constant.fullViewYPosition) / -velocity.y) : Double((Constant.partialViewYPosition - minY) / velocity.y)
            duration = duration > 1.3 ? 1 : duration
            
            UIView.animate(withDuration: duration, delay: 0.0, options: [.allowUserInteraction], animations: {
                if  velocity.y >= 0 {
                    self.view.frame = CGRect(x: 0, y: Constant.partialViewYPosition, width: self.view.frame.width, height: self.view.frame.height)
                    self.tableView.isScrollEnabled = false
                } else {
                    self.view.frame = CGRect(x: 0, y: Constant.fullViewYPosition, width: self.view.frame.width, height: self.view.frame.height)
                    self.tableView.isScrollEnabled = true
                }
            }, completion: nil)
        }
    }
}

extension BottomViewSheet:  UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FirstTableViewCell", for: indexPath) as! FirstTableViewCell
        cell.collectionView.reloadData()
        return cell
    }
}
