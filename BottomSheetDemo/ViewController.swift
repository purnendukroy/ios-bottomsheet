//
//  ViewController.swift
//  BottomSheetDemo
//
//  Created by Purnendu Roy on 26/03/20.
//  Copyright © 2020 Purnendu Roy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var unlockView: UIView!
    @IBOutlet weak var unlockButton: UIButtonX!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UserDefaults.standard.set(self.unlockView.frame.height, forKey: "UNLOCK_BUTTON_VIEW_HEIGHT")
        self.addBottomSheetView()
    }
    
    private func addBottomSheetView() {
        let bottomSheetVC = UIStoryboard(name: "BottomSheet", bundle: nil).instantiateViewController(withIdentifier: "BottomViewSheet")
        self.add(bottomSheetVC, sendSubviewToBack: true)
        
        //Adjust bottomSheet frame and initial position.
        bottomSheetVC.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: self.view.frame.width, height: (self.view.frame.height - 160))
    }
}

@nonobjc extension UIViewController {
    func add(_ child: UIViewController, frame: CGRect? = nil, sendSubviewToBack: Bool = false) {
        self.addChild(child) //add any child view controller as a child view

        if let frame = frame {
            child.view.frame = frame
        }

        self.view.addSubview(child.view)
        if sendSubviewToBack {
            self.view.sendSubviewToBack(child.view) //bring chid view to the back
        }
        child.didMove(toParent: self)
    }

    func remove() {
        // Just to be safe, we check that this view controller
        // is actually added to a parent before removing it.
        guard parent != nil else {
            return
        }
        self.willMove(toParent: nil)
        self.view.removeFromSuperview()
        self.removeFromParent()
    }
}
